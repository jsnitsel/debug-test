#!/bin/bash

# some io stress to exercise the iommu / dma-api code paths

if [[ "$#" != "1" ]];
then
    echo "Usage: stressor.sh #-of-test-loops"
fi

# make sure fio and stress-ng are installed
if ! test -f /usr/bin/fio;
then
    dnf install -y fio
fi

if ! test -f /usr/bin/stress-ng;
then
    dnf install -y stress-ng
fi

mkdir -p /home/test/stress

for i in $(seq 1 $1);
do
    if test -f $(pwd)/.stopit;
    then
	rm -f $(pwd)/.stopit
	echo "sentry file found...exiting"
	exit 0
    fi
       
    echo "beginning test loop $i/$1"

    for ho in direct dsync;
    do
	echo "running hdd stressor with $ho and readahead stressors"
	stress-ng --aggressive --verify --timeout 240 --temp-path /home/test/stress/ --hdd $(($(nproc) / 2)) --hdd-opts $ho --hdd-bytes 10% --readahead-bytes 16M -k --readahead $(($(nproc) / 2))
    done
    sleep 5

    echo "running iomix stressors"
    stress-ng --aggressive --verify --timeout 240 --temp-path /home/test/stress/ --iomix $(nproc) --iomix-bytes 10%
    sleep 5
    

    echo "running aio and aiol stressors"
    stress-ng --aggressive --verify --timeout 240 --temp-path /home/test/stress/ --aio $(($(nproc)/2)) --aiol $(($(nproc)/2))
    sleep 5

    mkdir -p /home/test/fio
    echo "running fio jobs"
    fio --name io-test --rwmixread 60 --rwmixwrite 40 --bsrange 4k-64k --iodepth=4 --size=10m --ioengine=libaio --direct=1 --time_based --runtime=240s --rw randrw --filename io-test --directory=/home/test/fio/ --numjobs=$(($(nproc) / 2))
    sleep 5
    rm -rf /home/test/fio
    
    echo "test loop $i/$1 complete"
done

rm -rf /home/test/stress
