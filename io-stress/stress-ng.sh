#!/bin/bash

if [[ "$#" != "1" ]];
then
    echo "Usage: stress-ng.sh #-of-test-loops"
fi

CPUS=$(($(nproc)/4))

sed -ie "s/NJOBS/${CPUS}/g" stress-job

for i in $(seq 1 $1);
do
    if test -f $(pwd)/.stopit;
    then
	rm -f $(pwd)/.stopit
	echo "sentry file found...exiting"
	exit 0
    fi
       
    echo "beginning test loop $i/$1"

    stress-ng --job ./stress-job

    echo "running fio jobs"
    mkdir -p /home/test/fio
    fio --name io-test --rwmixread 60 --rwmixwrite 40 --bsrange 4k-64k --iodepth=8 --size=10m --ioengine=libaio --direct=1 --time_based --runtime=240s --rw randrw --filename io-test --directory=/home/test/fio --numjobs=$(($(nproc) / 4))
    sleep 5
    rm -rf /home/test/fio
    sleep 5
    
    echo "test loop $i/$1 complete"
done

