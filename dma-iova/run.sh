#!/bin/sh

if [[ "$#" != "4" ]];
then
    echo "USAGE: $0 PF PCI-DEVFN BPFTRACE-FILE IPERF3-SERVER"
    exit 1
fi

if ! test -f /usr/bin/iperf3;
then
    echo "Please install iperf3: dnf install iperf3"
    exit 1
fi

if ! test -f /usr/bin/bpftrace;
then
    echo "Please install bpftrace: dnf install bpftrace"
    exit 1
fi

./map.sh $1 $2 $3 $4 >> output.txt 2>&1
