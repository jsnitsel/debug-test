#!/usr/bin/sh -xv

# $1 - PF
# $2 - PCI DEVFN
# $3 - bpftrace file
# $4 - system running iperf3 server

if [[ "$#" != "4" ]];
then
    echo "USAGE: $0 PF PCI-DEVFN BPFTRACE-FILE IPERF3-SERVER"
    exit 1
fi

#PF="enp33s0f0np0"
#PCI="0000:21:00.0"

PF=$1
PCI=$2
BPFT=$3

#systemctl stop NetworkManager

cat /proc/cmdline

# # allow testing with buffers up to 128MB
# sysctl net.core.rmem_max=134217728 
# sysctl net.core.wmem_max=134217728 
# # increase Linux autotuning TCP buffer limit to 64MB
# sysctl net.ipv4.tcp_rmem="4096 87380 67108864"
# sysctl net.ipv4.tcp_wmem="4096 65536 67108864"
# # recommended default congestion control is htcp 
# #sysctl net.ipv4.tcp_congestion_control=htcp
# # recommended for hosts with jumbo frames enabled
# sysctl net.ipv4.tcp_mtu_probing=1
# # recommended to enable 'fair queueing'
# sysctl net.core.default_qdisc=fq

# values copied from sos reports 

sysctl net.core.default_qdisc=fq
sysctl net.core.rmem_max=16777216
sysctl net.core.wmem_max=536870912
sysctl net.ipv4.tcp_congestion_control=cubic
sysctl net.ipv4.tcp_mtu_probing=1
sysctl net.ipv4.tcp_rmem="4096	87380	16777216"
sysctl net.ipv4.tcp_wmem="4096	65536	268435456"


sleep 2

rmmod mlx5_ib
rmmod mlx5_core

sleep 5

# ./${BPFT} $PCI ${#PCI} &

sleep 5

modprobe mlx5_core

sleep 5

echo "setting rx 2048 tx 1024 and starting iperf3 test" > /dev/kmsg

ethtool -G $PF rx 2048 tx 1024
ip l set dev $PF mtu 8900
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 10

iperf3 -i 10 -t 120 -P 8 -w16M --bidir -c $4

sleep 10

echo "setting rx 8192 tx 8192 and starting iperf3 test" > /dev/kmsg

ip l set dev $PF down
ethtool -G $PF rx 8192 tx 8192
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 10

iperf3 -i 10 -t 120 -P 8 -w16M --bidir -c $4

sleep 10

echo "setting rx 2048 tx 1024 and starting iperf3 test" > /dev/kmsg

ip l set dev $PF down
ethtool -G $PF rx 2048 tx 1024
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 10

iperf3 -i 10 -t 120 -P 8 -w16M --bidir -c $4

sleep 10

echo "setting rx 8192 tx 8192 and starting iperf3 test" > /dev/kmsg

ip l set dev $PF down
ethtool -G $PF rx 8192 tx 8192
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 10

iperf3 -i 10 -t 120 -P 8 -w16M --bidir -c $4

#ip link set dev $PF down
#sleep 5

#rmmod mlx5_ib
#rmmod mlx5_core
sleep 5

# kill %1
# wait
