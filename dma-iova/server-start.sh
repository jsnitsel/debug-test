#!/usr/bin/sh -xv

# $1 - PF

if [[ "$#" != "1" ]];
then
    echo "USAGE: $0 PF"
    exit 1
fi

PF=$1

# sysctl settings copied from sos report

sysctl net.core.default_qdisc=fq
sysctl net.core.rmem_max=16777216
sysctl net.core.wmem_max=536870912
sysctl net.ipv4.tcp_congestion_control=cubic
sysctl net.ipv4.tcp_mtu_probing=1
sysctl net.ipv4.tcp_rmem="4096	87380	16777216"
sysctl net.ipv4.tcp_wmem="4096	65536	268435456"


ethtool -G $PF rx 2048 tx 1024
ip l set dev $PF mtu 8900
ip l set dev $PF up
ethtool -g $PF
ethtool -l $PF

sleep 10

iperf3 -i 10 -s
