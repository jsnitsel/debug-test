#!/bin/bash

# $1 - target drive
# $2 - number of partitions
# $3 - partition type (ext4, xfs)
# $4 - number of iterations to run main test loop
#
# example: nvme-format-partition-test.sh /dev/nvme1n1 128 xfs 5
#
# To kill the test run during the middle, touch .stopit in
# the directory where it is running, and it will exit at
# the beginning of the next iteration

clear_parts() {
    local parts=$(grep -c '$2p*' /proc/partitions)
    for p in $(seq 1 $parts);
    do
	parted -s $1/$2 rm $p
    done    
}

format_drive() {
    cmd="nvme format --force $1 -s $2 -l $3 -i $4 -p $5"
    echo "formatting with command: ${cmd}" > /dev/kmsg
    nvme format --force $1 -s $2 -l $3 -i $4 -p $5
}

create_table() {
    parted --script $1 mklabel gpt
}

# figure out partition size
get_part_size() {
  local result=$(parted --script $1 unit MiB print | grep ^Disk | grep MiB | awk '{print $3}' | tr -d MiB)
  local ps=$((($result-1024)/$2))
  echo $ps
}

create_partitions() {
  local partsize=$(($(get_part_size $2 $3)))
  local start=1024
  local end=$(($start + $partsize))

  for p in $(seq 1 $3);
  do
      parted -a optimal -s /dev/$1 unit MiB mkpart $4 $start $end
      start=$end
      end=$(($start + $partsize))
  done    
}

run_fio() {
    for p in $(seq 1 $3);
    do
	fio --name testing-$1p$p --rwmixread 60 --rwmixwrite 40 --bsrange 4k-64k --iodepth=48 --size=100% --ioengine=libaio --direct=1 --time_based --runtime 60s --rw randrw --filename $2p$p > /tmp/$1p$p.log 2>&1 &
    done

    # wait for fio jobs to finish
    wait
}

fstypes=("ext4" "xfs")

fstype_check() {
    for f in ${fstypes[@]};
    do
	if [[ "$f" == "$1" ]];
	then
	    return
	fi
    done
    echo "FS type must be one of: ${fstypes[@]}"
    exit 1
}

# Usage
if [[ "$#" -ne "4" ]]
then
    echo "Usage: $0 DRIVE NUM-PARTS FS-TYPE TEST-RUNS"
    echo "   DRIVE - path of drive to use (include namespace, ie: /dev/nvme1n1)"
    echo "   NUM-PARTS - number of partitions to create"
    echo "   FS-TYPE - fs type for partition: ${fstypes[@]}"
    echo "   TEST-RUNS - number of times to loop through test cycle"
    exit 1
fi

# clean up sentry file
if test -f $(pwd)/.stopit;
then
    echo "Cleaning up sentry file from previous run"
    rm -f $(pwd)/.stopit
fi

drivefull=$1
drivepath=$(dirname $1)
drive=$(basename $1)
ctrl=${drive:0:${#drive}-2}

# Validate arguments
# - check drive is valid name and not mounted
# - ensure that number of partitions being created is range of 1 and 128
# - verify valid partition type
# - verify test runs is positive integer
mounts=$(grep -c $drive /proc/mounts)
if [[ "$mounts" -gt "0" ]];
then
    echo $1 has mounted partitions!!!
    exit 1
fi

if [[ "$2" -lt "1" || "$2" -gt "128" ]];
then
    echo "Number of partitions must be between in the range of [1,128]"
    exit 1
fi

if [[ "$4" -lt "1" ]];
then
    echo "Test runs should be a positive integer"
    exit 1
fi

fstype_check $3

# determine ses cap
# nvme id-ctrl
fna=$(nvme id-ctrl $drivepath/$ctrl | grep fna | awk '{print $3}')
sescap=$(($fna & 4))
if [[ "" == "" ]];
then
    ses=(1 2)
else
    ses=(1)
fi

# determine pi cap
# nvme id-ns

# determine pil cap
# nvme id-ns
dpc=$(nvme id-ns $drivepath/$drive | grep dpc | awk '{print $3}')
pilcap=$(($dpc & 0x18))
picap=$(($dpc & 0x7))

case $pilcap in
    24)
	pil=(0 1)
	;;
    8)
	pil=(1)
	;;
    *)
	pil=(0)
	;;
esac

case $picap in
    7)
	pi=(1 2 3)
	;;
    6)
	pi=(2 3)
	;;
    5)
	pi=(1 3)
	;;
    4)
	pi=(3)
	;;
    3)
	pi=(1 2)
	;;
    2)
	pi=(2)
	;;
    1)
	pi=(1)
	;;
    *)
	pi=(0)
	;;
esac

# determine lba formats which support metadata
nlbaf=$(nvme id-ns $drivepath/$drive | grep ^nlbaf | awk '{print $3}')
lbafs=()
for x in $(seq 0 $nlbaf);
do
    ms=$(nvme id-ns $drivepath/$drive | grep "^lbaf  ${x}" | awk '{print $4}' | tr -d ms:)
    if [[ "$ms" -gt "0" ]];
    then
	lbafs+=($x)
    fi
done

# test loop
for t in $(seq 1 $4);
do
    echo "Starting test loop ${t}" > /dev/kmsg
    if test -f $(pwd)/.stopit;
    then
	echo "Sentry file found. Exiting test loop..."
	break
    fi

    # run without secure settings first
    clear_parts $drivepath $drive
    format_drive $drivefull 0 0 0 0
    create_table $drivefull
    create_partitions $drive $drivefull $2 $3
    run_fio $drive $drivefull $2
    
    # --ses (-s)
    for s in ${ses[@]};
    do
	for l in ${lbafs[@]};
	do
	    # --pi -i
	    for i in ${pi[@]};
	    do
		# --pil -p
		for p in ${pil[@]};
		do
		    clear_parts $drivepath $drive
		    format_drive $drivefull $s $l $i $p
		    create_table $drivefull
		    create_partitions $drive $drivefull $2 $3
		    run_fio $drive $drivefull $2
		done
	    done
	done
    done
    echo "Test loop ${t} complete" > /dev/kmsg
done    

# final clean up
clear_parts $drive

echo "done!"
exit 0
